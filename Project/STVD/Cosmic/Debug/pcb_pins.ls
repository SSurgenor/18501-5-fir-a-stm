   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
  56                     ; 72 void pcb_pins(uint8_t enable)
  56                     ; 73 {
  58                     	switch	.text
  59  0000               _pcb_pins:
  63                     ; 81 	if(enable){
  65  0000 4d            	tnz	a
  66  0001 2716          	jreq	L72
  67                     ; 83 		GPIO_Init(GPIOA,	GPIO_PIN_3,		GPIO_MODE_OUT_PP_LOW_FAST);	// TP
  69  0003 4be0          	push	#224
  70  0005 4b08          	push	#8
  71  0007 ae5000        	ldw	x,#20480
  72  000a cd0000        	call	_GPIO_Init
  74  000d 85            	popw	x
  75                     ; 100 			GPIO_Init(GPIOC,	GPIO_PIN_5,		GPIO_MODE_OUT_PP_LOW_FAST);	// FET_EN
  77  000e 4be0          	push	#224
  78  0010 4b20          	push	#32
  79  0012 ae500a        	ldw	x,#20490
  80  0015 cd0000        	call	_GPIO_Init
  82  0018 85            	popw	x
  83  0019               L72:
  84                     ; 121 	return;
  87  0019 81            	ret
 100                     	xdef	_pcb_pins
 101                     	xref	_GPIO_Init
 120                     	end
