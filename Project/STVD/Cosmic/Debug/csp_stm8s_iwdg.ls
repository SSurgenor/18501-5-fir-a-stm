   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
   4                     ; Optimizer V4.3.6 - 29 Nov 2011
  55                     ; 38 void csp_stm8s_iwdg_init(void)
  55                     ; 39 {
  57                     .text:	section	.text,new
  58  0000               _csp_stm8s_iwdg_init:
  62                     ; 42 	csp_STM8S_clk_LSI(1);							//internal 128kHz clk enable
  64  0000 a601          	ld	a,#1
  65  0002 cd0000        	call	_csp_STM8S_clk_LSI
  67                     ; 44 	IWDG_WriteAccessCmd(IWDG_WRITE_ACCESS);			//write access command
  69  0005 a655          	ld	a,#85
  70  0007 cd0000        	call	_IWDG_WriteAccessCmd
  72                     ; 46 	IWDG_SetPrescaler(IWDG_Prescaler_128);			//set prescalar   
  74  000a a605          	ld	a,#5
  75  000c cd0000        	call	_IWDG_SetPrescaler
  77                     ; 50 	IWDG_SetReload(100);							//set reload
  79  000f a664          	ld	a,#100
  80  0011 cd0000        	call	_IWDG_SetReload
  82                     ; 52 	csp_stm8s_iwdg_ReloadCounter();					//comand to reload the counddown timer
  84  0014 cd0000        	call	_csp_stm8s_iwdg_ReloadCounter
  86                     ; 53 	IWDG_Enable();
  89                     ; 55 	return;
  92  0017 cc0000        	jp	_IWDG_Enable
 116                     ; 69 void csp_stm8s_iwdg_ReloadCounter(void)
 116                     ; 70 {
 117                     .text:	section	.text,new
 118  0000               _csp_stm8s_iwdg_ReloadCounter:
 122                     ; 73   IWDG->KR = IWDG_KEY_REFRESH;
 124  0000 35aa50e0      	mov	20704,#170
 125                     ; 74 }
 128  0004 81            	ret	
 141                     	xref	_csp_STM8S_clk_LSI
 142                     	xdef	_csp_stm8s_iwdg_ReloadCounter
 143                     	xdef	_csp_stm8s_iwdg_init
 144                     	xref	_IWDG_Enable
 145                     	xref	_IWDG_SetReload
 146                     	xref	_IWDG_SetPrescaler
 147                     	xref	_IWDG_WriteAccessCmd
 166                     	end
