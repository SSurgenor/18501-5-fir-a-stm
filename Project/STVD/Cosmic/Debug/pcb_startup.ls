   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
  45                     ; 61 void pcb_startup(void)
  45                     ; 62 {
  47                     	switch	.text
  48  0000               _pcb_startup:
  52                     ; 66 	pcb_pins(1);
  54  0000 a601          	ld	a,#1
  55  0002 cd0000        	call	_pcb_pins
  57                     ; 69 	return;
  60  0005 81            	ret
  73                     	xdef	_pcb_startup
  74                     	xref	_pcb_pins
  93                     	end
