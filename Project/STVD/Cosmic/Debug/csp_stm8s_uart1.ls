   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
   4                     ; Optimizer V4.3.6 - 29 Nov 2011
  55                     ; 45 void csp_STM8S_uart1_config(void)
  55                     ; 46 {
  57                     .text:	section	.text,new
  58  0000               _csp_STM8S_uart1_config:
  62                     ; 47 	CLK_PeripheralClockConfig(CLK_PERIPHERAL_UART1, ENABLE);
  64  0000 ae0301        	ldw	x,#769
  65  0003 cd0000        	call	_CLK_PeripheralClockConfig
  67                     ; 49 	UART1_Init(	BAUD_RATE,
  67                     ; 50 				UART1_WORDLENGTH_8D,
  67                     ; 51 				UART1_STOPBITS_1,
  67                     ; 52 				UART1_PARITY_NO,
  67                     ; 53 				UART1_SYNCMODE_CLOCK_DISABLE,
  67                     ; 54 				UART1_MODE_TXRX_ENABLE);
  69  0006 4b0c          	push	#12
  70  0008 4b80          	push	#128
  71  000a 4b00          	push	#0
  72  000c 4b00          	push	#0
  73  000e 4b00          	push	#0
  74  0010 ae2580        	ldw	x,#9600
  75  0013 89            	pushw	x
  76  0014 5f            	clrw	x
  77  0015 89            	pushw	x
  78  0016 cd0000        	call	_UART1_Init
  80  0019 5b09          	addw	sp,#9
  81                     ; 56 	st_uart1_rx_glb.rd_index_u8		=  0;
  83  001b 3f0b          	clr	_st_uart1_rx_glb+1
  84                     ; 57 	st_uart1_rx_glb.wr_index_u8		=  0;
  86  001d 3f0a          	clr	_st_uart1_rx_glb
  87                     ; 58 	st_uart1_rx_glb.cnt_u8			=  0;
  89  001f 3f0c          	clr	_st_uart1_rx_glb+2
  90                     ; 61   	UART1_ITConfig(UART1_IT_RXNE, ENABLE);			// this interrupt is generated when the USART receive data register is not empty
  92  0021 4b01          	push	#1
  93  0023 ae0255        	ldw	x,#597
  94  0026 cd0000        	call	_UART1_ITConfig
  96  0029 84            	pop	a
  97                     ; 66 	UART1_Cmd(ENABLE);
  99  002a a601          	ld	a,#1
 100  002c cd0000        	call	_UART1_Cmd
 102                     ; 68 	enableInterrupts();										// Enable general interrupts
 105  002f 9a            	rim	
 107                     ; 69 	return;
 111  0030 81            	ret	
 139                     ; 84 void csp_STM8S_uart_rx_interrupt_handler(void)
 139                     ; 85 {
 140                     .text:	section	.text,new
 141  0000               _csp_STM8S_uart_rx_interrupt_handler:
 145                     ; 86 	uart1_rx_buffer_a_u8_glb[st_uart1_rx_glb.wr_index_u8]	=  UART1->DR;
 147  0000 b60a          	ld	a,_st_uart1_rx_glb
 148  0002 5f            	clrw	x
 149  0003 97            	ld	xl,a
 150  0004 c65231        	ld	a,21041
 151  0007 e700          	ld	(_uart1_rx_buffer_a_u8_glb,x),a
 152                     ; 87 	if(++st_uart1_rx_glb.wr_index_u8 >= UART1_RX_BUFFER_SIZE){
 154  0009 3c0a          	inc	_st_uart1_rx_glb
 155  000b b60a          	ld	a,_st_uart1_rx_glb
 156  000d a10a          	cp	a,#10
 157  000f 2502          	jrult	L13
 158                     ; 88 		st_uart1_rx_glb.wr_index_u8	=  0;
 160  0011 3f0a          	clr	_st_uart1_rx_glb
 161  0013               L13:
 162                     ; 90 	disableInterrupts();
 165  0013 9b            	sim	
 167                     ; 91 	st_uart1_rx_glb.cnt_u8++;
 170  0014 3c0c          	inc	_st_uart1_rx_glb+2
 171                     ; 92 	enableInterrupts();
 174  0016 9a            	rim	
 176                     ; 94 }
 180  0017 81            	ret	
 229                     ; 108 uint8_t csp_STM8S_uart1_getchar(uint8_t *rec_status_u8)
 229                     ; 109 {
 230                     .text:	section	.text,new
 231  0000               _csp_STM8S_uart1_getchar:
 233  0000 89            	pushw	x
 234  0001 88            	push	a
 235       00000001      OFST:	set	1
 238                     ; 110 	uint8_t 		data_byte_u8 	= 0;
 240  0002 0f01          	clr	(OFST+0,sp)
 241                     ; 112 	*rec_status_u8	= 0;
 243  0004 7f            	clr	(x)
 244                     ; 114 	if(st_uart1_rx_glb.cnt_u8 != 0){
 246  0005 b60c          	ld	a,_st_uart1_rx_glb+2
 247  0007 271b          	jreq	L55
 248                     ; 115 		data_byte_u8		= uart1_rx_buffer_a_u8_glb[st_uart1_rx_glb.rd_index_u8];
 250  0009 b60b          	ld	a,_st_uart1_rx_glb+1
 251  000b 5f            	clrw	x
 252  000c 97            	ld	xl,a
 253  000d e600          	ld	a,(_uart1_rx_buffer_a_u8_glb,x)
 254  000f 6b01          	ld	(OFST+0,sp),a
 255                     ; 116 		*rec_status_u8		= 1;
 257  0011 1e02          	ldw	x,(OFST+1,sp)
 258  0013 a601          	ld	a,#1
 259  0015 f7            	ld	(x),a
 260                     ; 118 		if (++st_uart1_rx_glb.rd_index_u8 >= UART1_RX_BUFFER_SIZE){
 262  0016 3c0b          	inc	_st_uart1_rx_glb+1
 263  0018 b60b          	ld	a,_st_uart1_rx_glb+1
 264  001a a10a          	cp	a,#10
 265  001c 2502          	jrult	L75
 266                     ; 119 			st_uart1_rx_glb.rd_index_u8	=  0;
 268  001e 3f0b          	clr	_st_uart1_rx_glb+1
 269  0020               L75:
 270                     ; 121 		disableInterrupts();
 273  0020 9b            	sim	
 275                     ; 122 		st_uart1_rx_glb.cnt_u8--;
 278  0021 3a0c          	dec	_st_uart1_rx_glb+2
 279                     ; 123 		enableInterrupts();
 282  0023 9a            	rim	
 285  0024               L55:
 286                     ; 127    	return( (char)data_byte_u8 );
 288  0024 7b01          	ld	a,(OFST+0,sp)
 291  0026 5b03          	addw	sp,#3
 292  0028 81            	ret	
 337                     ; 142 void csp_STM8S_uart1_putchar(uint8_t out_char)
 337                     ; 143 {
 338                     .text:	section	.text,new
 339  0000               _csp_STM8S_uart1_putchar:
 341       00000001      OFST:	set	1
 344                     ; 147 	UART1->CR2 &= ~UART1_CR2_REN;	//0 disabled/1 enabled transmitter
 346  0000 72155235      	bres	21045,#2
 347  0004 88            	push	a
 348                     ; 148 	UART1->CR2 |=  UART1_CR2_TEN;	//0 disabled/1 enabled transmitter
 350  0005 72165235      	bset	21045,#3
 351                     ; 152 	UART1_SendData8(out_char);
 353  0009 cd0000        	call	_UART1_SendData8
 355  000c               L301:
 356                     ; 155 		status	=  UART1->SR;
 358  000c c65230        	ld	a,21040
 359  000f 6b01          	ld	(OFST+0,sp),a
 360                     ; 156 		status	&= UART1_FLAG_TC;
 362  0011 7b01          	ld	a,(OFST+0,sp)
 363  0013 a440          	and	a,#64
 364  0015 6b01          	ld	(OFST+0,sp),a
 365                     ; 157 	}while(status == 0);
 367  0017 0d01          	tnz	(OFST+0,sp)
 368  0019 27f1          	jreq	L301
 369                     ; 161 	UART1->CR2 &= ~UART1_CR2_TEN;	//0 disabled/1 enabled transmitter
 371  001b 72175235      	bres	21045,#3
 372                     ; 162 	UART1->CR2 |=  UART1_CR2_REN;	//0 disabled/1 enabled transmitter
 374                     ; 164    	return;
 377  001f 84            	pop	a
 378  0020 72145235      	bset	21045,#2
 379  0024 81            	ret	
 416                     ; 178 void csp_STM8S_uart1_putstr(char *str)
 416                     ; 179 {
 417                     .text:	section	.text,new
 418  0000               _csp_STM8S_uart1_putstr:
 420  0000 89            	pushw	x
 421       00000000      OFST:	set	0
 424  0001 1e01          	ldw	x,(OFST+1,sp)
 425  0003 f6            	ld	a,(x)
 426  0004               L721:
 427                     ; 181 		csp_STM8S_uart1_putchar(*str);
 429  0004 cd0000        	call	_csp_STM8S_uart1_putchar
 431                     ; 182 		str++;
 433  0007 1e01          	ldw	x,(OFST+1,sp)
 434  0009 5c            	incw	x
 435  000a 1f01          	ldw	(OFST+1,sp),x
 436                     ; 183 	}while(*str != 0x00);
 438  000c f6            	ld	a,(x)
 439  000d 26f5          	jrne	L721
 440                     ; 185    	return;
 443  000f 85            	popw	x
 444  0010 81            	ret	
 526                     ; 200 void csp_STM8S_uart1_putnum(uint32_t num,uint8_t base,uint8_t len)
 526                     ; 201 {
 527                     .text:	section	.text,new
 528  0000               _csp_STM8S_uart1_putnum:
 530  0000 5212          	subw	sp,#18
 531       00000012      OFST:	set	18
 534                     ; 206 	for(i=len;i>0;i--){
 536  0002 7b1a          	ld	a,(OFST+8,sp)
 537  0004 6b12          	ld	(OFST+0,sp),a
 539  0006 2055          	jra	L302
 540  0008               L771:
 541                     ; 207 		temp	=  num%base;
 543  0008 7b19          	ld	a,(OFST+7,sp)
 544  000a b703          	ld	c_lreg+3,a
 545  000c 3f02          	clr	c_lreg+2
 546  000e 3f01          	clr	c_lreg+1
 547  0010 3f00          	clr	c_lreg
 548  0012 96            	ldw	x,sp
 549  0013 5c            	incw	x
 550  0014 cd0000        	call	c_rtol
 552  0017 96            	ldw	x,sp
 553  0018 1c0015        	addw	x,#OFST+3
 554  001b cd0000        	call	c_ltor
 556  001e 96            	ldw	x,sp
 557  001f 5c            	incw	x
 558  0020 cd0000        	call	c_lumd
 560  0023 96            	ldw	x,sp
 561  0024 1c0005        	addw	x,#OFST-13
 562  0027 cd0000        	call	c_rtol
 564                     ; 208 		num		/= base;
 566  002a 7b19          	ld	a,(OFST+7,sp)
 567  002c b703          	ld	c_lreg+3,a
 568  002e 3f02          	clr	c_lreg+2
 569  0030 3f01          	clr	c_lreg+1
 570  0032 3f00          	clr	c_lreg
 571  0034 96            	ldw	x,sp
 572  0035 5c            	incw	x
 573  0036 cd0000        	call	c_rtol
 575  0039 96            	ldw	x,sp
 576  003a 1c0015        	addw	x,#OFST+3
 577  003d cd0000        	call	c_ltor
 579  0040 96            	ldw	x,sp
 580  0041 5c            	incw	x
 581  0042 cd0000        	call	c_ludv
 583  0045 96            	ldw	x,sp
 584  0046 1c0015        	addw	x,#OFST+3
 585  0049 cd0000        	call	c_rtol
 587                     ; 209 		str[i]	=  (char)temp;
 589  004c 96            	ldw	x,sp
 590  004d 1c0009        	addw	x,#OFST-9
 591  0050 9f            	ld	a,xl
 592  0051 5e            	swapw	x
 593  0052 1b12          	add	a,(OFST+0,sp)
 594  0054 2401          	jrnc	L43
 595  0056 5c            	incw	x
 596  0057               L43:
 597  0057 02            	rlwa	x,a
 598  0058 7b08          	ld	a,(OFST-10,sp)
 599  005a f7            	ld	(x),a
 600                     ; 206 	for(i=len;i>0;i--){
 602  005b 0a12          	dec	(OFST+0,sp)
 603  005d               L302:
 606  005d 26a9          	jrne	L771
 607                     ; 211 	for(i=1;i<=len;i++){
 609  005f a601          	ld	a,#1
 610  0061 6b12          	ld	(OFST+0,sp),a
 612  0063 2037          	jra	L312
 613  0065               L702:
 614                     ; 212 		if(str[i] <= 9){	csp_STM8S_uart1_putchar(str[i]+'0');	}
 616  0065 96            	ldw	x,sp
 617  0066 1c0009        	addw	x,#OFST-9
 618  0069 9f            	ld	a,xl
 619  006a 5e            	swapw	x
 620  006b 1b12          	add	a,(OFST+0,sp)
 621  006d 2401          	jrnc	L63
 622  006f 5c            	incw	x
 623  0070               L63:
 624  0070 02            	rlwa	x,a
 625  0071 f6            	ld	a,(x)
 626  0072 a10a          	cp	a,#10
 627  0074 96            	ldw	x,sp
 628  0075 2410          	jruge	L712
 631  0077 1c0009        	addw	x,#OFST-9
 632  007a 9f            	ld	a,xl
 633  007b 5e            	swapw	x
 634  007c 1b12          	add	a,(OFST+0,sp)
 635  007e 2401          	jrnc	L24
 636  0080 5c            	incw	x
 637  0081               L24:
 638  0081 02            	rlwa	x,a
 639  0082 f6            	ld	a,(x)
 640  0083 ab30          	add	a,#48
 643  0085 200e          	jra	L122
 644  0087               L712:
 645                     ; 213 		else{				csp_STM8S_uart1_putchar(str[i]+'A'-10);	}
 647  0087 1c0009        	addw	x,#OFST-9
 648  008a 9f            	ld	a,xl
 649  008b 5e            	swapw	x
 650  008c 1b12          	add	a,(OFST+0,sp)
 651  008e 2401          	jrnc	L64
 652  0090 5c            	incw	x
 653  0091               L64:
 654  0091 02            	rlwa	x,a
 655  0092 f6            	ld	a,(x)
 656  0093 ab37          	add	a,#55
 658  0095               L122:
 659  0095 cd0000        	call	_csp_STM8S_uart1_putchar
 660                     ; 211 	for(i=1;i<=len;i++){
 662  0098 0c12          	inc	(OFST+0,sp)
 663  009a 7b12          	ld	a,(OFST+0,sp)
 664  009c               L312:
 667  009c 111a          	cp	a,(OFST+8,sp)
 668  009e 23c5          	jrule	L702
 669                     ; 215    	return;
 672  00a0 5b12          	addw	sp,#18
 673  00a2 81            	ret	
 746                     ; 228 uint32_t csp_STM8S_uart1_getnum(uint8_t base)
 746                     ; 229 {
 747                     .text:	section	.text,new
 748  0000               _csp_STM8S_uart1_getnum:
 750  0000 88            	push	a
 751  0001 5207          	subw	sp,#7
 752       00000007      OFST:	set	7
 755                     ; 230 	uint32_t	num			=  0;
 757  0003 5f            	clrw	x
 758  0004 1f06          	ldw	(OFST-1,sp),x
 759  0006 1f04          	ldw	(OFST-3,sp),x
 760                     ; 233 	uint8_t		non_dig		=  0;
 762  0008 0f02          	clr	(OFST-5,sp)
 763  000a               L162:
 764                     ; 236 		rx_byte		=  csp_STM8S_uart1_getchar(&rx_status);
 766  000a 96            	ldw	x,sp
 767  000b 5c            	incw	x
 768  000c cd0000        	call	_csp_STM8S_uart1_getchar
 770  000f 6b03          	ld	(OFST-4,sp),a
 771                     ; 237 		if(rx_status){
 773  0011 0d01          	tnz	(OFST-6,sp)
 774  0013 2603cc00bc    	jreq	L362
 775                     ; 238 			if((rx_byte >= '0')&&(rx_byte <= '9')){
 777  0018 a130          	cp	a,#48
 778  001a 252b          	jrult	L172
 780  001c a13a          	cp	a,#58
 781  001e 2427          	jruge	L172
 782                     ; 239 				num		*= base;
 784  0020 7b08          	ld	a,(OFST+1,sp)
 785  0022 b703          	ld	c_lreg+3,a
 786  0024 3f02          	clr	c_lreg+2
 787  0026 3f01          	clr	c_lreg+1
 788  0028 3f00          	clr	c_lreg
 789  002a 96            	ldw	x,sp
 790  002b 1c0004        	addw	x,#OFST-3
 791  002e cd0000        	call	c_lgmul
 793                     ; 240 				num		+= rx_byte;
 795  0031 7b03          	ld	a,(OFST-4,sp)
 796  0033 96            	ldw	x,sp
 797  0034 1c0004        	addw	x,#OFST-3
 798  0037 88            	push	a
 799  0038 cd0000        	call	c_lgadc
 801  003b 84            	pop	a
 802                     ; 241 				num		-= '0';
 804  003c 96            	ldw	x,sp
 805  003d 1c0004        	addw	x,#OFST-3
 806  0040 a630          	ld	a,#48
 807  0042 cd0000        	call	c_lgsbc
 810  0045 2070          	jra	L372
 811  0047               L172:
 812                     ; 243 			else if(base == 16){
 814  0047 7b08          	ld	a,(OFST+1,sp)
 815  0049 a110          	cp	a,#16
 816  004b 2666          	jrne	L303
 817                     ; 244 				if((rx_byte >= 'A')&&(rx_byte <= 'F')){
 819  004d 7b03          	ld	a,(OFST-4,sp)
 820  004f a141          	cp	a,#65
 821  0051 2528          	jrult	L772
 823  0053 a147          	cp	a,#71
 824  0055 2424          	jruge	L772
 825                     ; 245 					num		*= base;
 827  0057 7b08          	ld	a,(OFST+1,sp)
 828  0059 b703          	ld	c_lreg+3,a
 829  005b 3f02          	clr	c_lreg+2
 830  005d 3f01          	clr	c_lreg+1
 831  005f 3f00          	clr	c_lreg
 832  0061 96            	ldw	x,sp
 833  0062 1c0004        	addw	x,#OFST-3
 834  0065 cd0000        	call	c_lgmul
 836                     ; 246 					num		+= rx_byte;
 838  0068 7b03          	ld	a,(OFST-4,sp)
 839  006a 96            	ldw	x,sp
 840  006b 1c0004        	addw	x,#OFST-3
 841  006e 88            	push	a
 842  006f cd0000        	call	c_lgadc
 844  0072 84            	pop	a
 845                     ; 247 					num		-= 'A';
 847  0073 96            	ldw	x,sp
 848  0074 1c0004        	addw	x,#OFST-3
 849  0077 a641          	ld	a,#65
 851                     ; 248 					num		+= 10;
 854  0079 202a          	jp	LC002
 855  007b               L772:
 856                     ; 250 				else if((rx_byte >= 'a')&&(rx_byte <= 'f')){
 858  007b a161          	cp	a,#97
 859  007d 2534          	jrult	L303
 861  007f a167          	cp	a,#103
 862  0081 2430          	jruge	L303
 863                     ; 251 					num		*= base;
 865  0083 7b08          	ld	a,(OFST+1,sp)
 866  0085 b703          	ld	c_lreg+3,a
 867  0087 3f02          	clr	c_lreg+2
 868  0089 3f01          	clr	c_lreg+1
 869  008b 3f00          	clr	c_lreg
 870  008d 96            	ldw	x,sp
 871  008e 1c0004        	addw	x,#OFST-3
 872  0091 cd0000        	call	c_lgmul
 874                     ; 252 					num		+= rx_byte;
 876  0094 7b03          	ld	a,(OFST-4,sp)
 877  0096 96            	ldw	x,sp
 878  0097 1c0004        	addw	x,#OFST-3
 879  009a 88            	push	a
 880  009b cd0000        	call	c_lgadc
 882  009e 84            	pop	a
 883                     ; 253 					num		-= 'a';
 885  009f 96            	ldw	x,sp
 886  00a0 1c0004        	addw	x,#OFST-3
 887  00a3 a661          	ld	a,#97
 889                     ; 254 					num		+= 10;
 891  00a5               LC002:
 892  00a5 cd0000        	call	c_lgsbc
 894  00a8 96            	ldw	x,sp
 895  00a9 1c0004        	addw	x,#OFST-3
 896  00ac a60a          	ld	a,#10
 897  00ae cd0000        	call	c_lgadc
 900  00b1 2004          	jra	L372
 901  00b3               L303:
 902                     ; 257 					non_dig	=  1;
 903                     ; 261 				non_dig	=  1;
 906  00b3 a601          	ld	a,#1
 907  00b5 6b02          	ld	(OFST-5,sp),a
 908  00b7               L372:
 909                     ; 263 			csp_STM8S_uart1_putchar(rx_byte);
 911  00b7 7b03          	ld	a,(OFST-4,sp)
 912  00b9 cd0000        	call	_csp_STM8S_uart1_putchar
 914  00bc               L362:
 915                     ; 265 	}while(non_dig == 0);
 917  00bc 7b02          	ld	a,(OFST-5,sp)
 918  00be 2603cc000a    	jreq	L162
 919                     ; 267    	return(num);
 921  00c3 96            	ldw	x,sp
 922  00c4 1c0004        	addw	x,#OFST-3
 923  00c7 cd0000        	call	c_ltor
 927  00ca 5b08          	addw	sp,#8
 928  00cc 81            	ret	
 992                     	xdef	_csp_STM8S_uart1_getnum
 993                     	xdef	_csp_STM8S_uart1_putnum
 994                     	xdef	_csp_STM8S_uart1_putstr
 995                     	xdef	_csp_STM8S_uart1_putchar
 996                     	xdef	_csp_STM8S_uart1_getchar
 997                     	xdef	_csp_STM8S_uart_rx_interrupt_handler
 998                     	xdef	_csp_STM8S_uart1_config
 999                     	switch	.ubsct
1000  0000               _uart1_rx_buffer_a_u8_glb:
1001  0000 000000000000  	ds.b	10
1002                     	xdef	_uart1_rx_buffer_a_u8_glb
1003  000a               _st_uart1_rx_glb:
1004  000a 000000        	ds.b	3
1005                     	xdef	_st_uart1_rx_glb
1006                     	xref	_UART1_SendData8
1007                     	xref	_UART1_ITConfig
1008                     	xref	_UART1_Cmd
1009                     	xref	_UART1_Init
1010                     	xref	_CLK_PeripheralClockConfig
1011                     	xref.b	c_lreg
1012                     	xref.b	c_x
1032                     	xref	c_lgsbc
1033                     	xref	c_lgadc
1034                     	xref	c_lgmul
1035                     	xref	c_ludv
1036                     	xref	c_lumd
1037                     	xref	c_rtol
1038                     	xref	c_ltor
1039                     	end
