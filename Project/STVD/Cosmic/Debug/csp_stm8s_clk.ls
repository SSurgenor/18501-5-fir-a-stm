   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
   4                     ; Optimizer V4.3.6 - 29 Nov 2011
  61                     ; 36 void csp_STM8S_clk_LSI(uint8_t state)
  61                     ; 37 {
  63                     .text:	section	.text,new
  64  0000               _csp_STM8S_clk_LSI:
  68                     ; 38 	if(state){
  70  0000 4d            	tnz	a
  71  0001 270f          	jreq	L72
  72                     ; 39 		CLK_LSICmd(ENABLE);										// Enable LSI
  74  0003 a601          	ld	a,#1
  75  0005 cd0000        	call	_CLK_LSICmd
  78  0008               L33:
  79                     ; 40 		while (CLK_GetFlagStatus(CLK_FLAG_LSIRDY) == RESET);	// Wait for LSI clock to be ready
  81  0008 ae0110        	ldw	x,#272
  82  000b cd0000        	call	_CLK_GetFlagStatus
  84  000e 4d            	tnz	a
  85  000f 27f7          	jreq	L33
  88  0011 81            	ret	
  89  0012               L72:
  90                     ; 43 		CLK_LSICmd(DISABLE);									// Disable LSI
  93                     ; 47 	return;
  96  0012 cc0000        	jp	_CLK_LSICmd
 132                     ; 60 void csp_STM8S_clk_HSE(uint8_t state)
 132                     ; 61 {
 133                     .text:	section	.text,new
 134  0000               _csp_STM8S_clk_HSE:
 138                     ; 62 	if(state){
 140  0000 4d            	tnz	a
 141  0001 270f          	jreq	L75
 142                     ; 63 		CLK_HSECmd(ENABLE);										// Enable HSE
 144  0003 a601          	ld	a,#1
 145  0005 cd0000        	call	_CLK_HSECmd
 148  0008               L36:
 149                     ; 64 		while (CLK_GetFlagStatus(CLK_FLAG_HSERDY) == RESET);	// Wait for HSE clock to be ready
 151  0008 ae0202        	ldw	x,#514
 152  000b cd0000        	call	_CLK_GetFlagStatus
 154  000e 4d            	tnz	a
 155  000f 27f7          	jreq	L36
 158  0011 81            	ret	
 159  0012               L75:
 160                     ; 67 		CLK_HSECmd(DISABLE);									// Disable HSE
 163                     ; 71 	return;
 166  0012 cc0000        	jp	_CLK_HSECmd
 202                     ; 84 void csp_STM8S_clk_HSI(uint8_t state)
 202                     ; 85 {
 203                     .text:	section	.text,new
 204  0000               _csp_STM8S_clk_HSI:
 208                     ; 86 	if(state){
 210  0000 4d            	tnz	a
 211  0001 270f          	jreq	L701
 212                     ; 87 		CLK_HSICmd(ENABLE);										// Enable HSI
 214  0003 a601          	ld	a,#1
 215  0005 cd0000        	call	_CLK_HSICmd
 218  0008               L311:
 219                     ; 88 		while (CLK_GetFlagStatus(CLK_FLAG_HSIRDY) == RESET);	// Wait for HSI clock to be ready
 221  0008 ae0102        	ldw	x,#258
 222  000b cd0000        	call	_CLK_GetFlagStatus
 224  000e 4d            	tnz	a
 225  000f 27f7          	jreq	L311
 228  0011 81            	ret	
 229  0012               L701:
 230                     ; 91 		CLK_HSICmd(DISABLE);									// Disable HSI
 233                     ; 95 	return;
 236  0012 cc0000        	jp	_CLK_HSICmd
 249                     	xdef	_csp_STM8S_clk_HSI
 250                     	xdef	_csp_STM8S_clk_HSE
 251                     	xdef	_csp_STM8S_clk_LSI
 252                     	xref	_CLK_GetFlagStatus
 253                     	xref	_CLK_LSICmd
 254                     	xref	_CLK_HSICmd
 255                     	xref	_CLK_HSECmd
 274                     	end
