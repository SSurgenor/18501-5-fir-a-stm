   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
   4                     ; Optimizer V4.3.6 - 29 Nov 2011
  54                     ; 36 void csp_stm8s_tim2_init(void)
  54                     ; 37 {
  56                     .text:	section	.text,new
  57  0000               _csp_stm8s_tim2_init:
  61                     ; 38 	CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER2, ENABLE);
  63  0000 ae0501        	ldw	x,#1281
  64  0003 cd0000        	call	_CLK_PeripheralClockConfig
  66                     ; 39 	TIM2_DeInit();
  68  0006 cd0000        	call	_TIM2_DeInit
  70                     ; 41 	TIM2_TimeBaseInit(TIM2_PRESCALER_16, 1000);
  72  0009 ae03e8        	ldw	x,#1000
  73  000c 89            	pushw	x
  74  000d a604          	ld	a,#4
  75  000f cd0000        	call	_TIM2_TimeBaseInit
  77  0012 85            	popw	x
  78                     ; 43 	TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
  80  0013 ae0101        	ldw	x,#257
  81  0016 cd0000        	call	_TIM2_ITConfig
  83                     ; 45 	TIM2_Cmd(ENABLE);		
  85  0019 a601          	ld	a,#1
  87                     ; 46 	return;
  90  001b cc0000        	jp	_TIM2_Cmd
 103                     	xdef	_csp_stm8s_tim2_init
 104                     	xref	_TIM2_ITConfig
 105                     	xref	_TIM2_Cmd
 106                     	xref	_TIM2_TimeBaseInit
 107                     	xref	_TIM2_DeInit
 108                     	xref	_CLK_PeripheralClockConfig
 127                     	end
