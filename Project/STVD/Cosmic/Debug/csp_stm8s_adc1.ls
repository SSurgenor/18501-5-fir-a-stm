   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
   4                     ; Optimizer V4.3.6 - 29 Nov 2011
  64                     ; 37 void csp_stm8s_adc1_init(uint8_t channel)
  64                     ; 38 {
  66                     .text:	section	.text,new
  67  0000               _csp_stm8s_adc1_init:
  69  0000 88            	push	a
  70       00000000      OFST:	set	0
  73                     ; 39 	CLK_PeripheralClockConfig(CLK_PERIPHERAL_ADC, ENABLE);
  75  0001 ae1301        	ldw	x,#4865
  76  0004 cd0000        	call	_CLK_PeripheralClockConfig
  78                     ; 40 	ADC1_DeInit();
  80  0007 cd0000        	call	_ADC1_DeInit
  82                     ; 42 	ADC1_Init(					ADC1_CONVERSIONMODE_SINGLE,
  82                     ; 43 								channel,
  82                     ; 44 								ADC1_PRESSEL_FCPU_D2,
  82                     ; 45 								ADC1_EXTTRIG_TIM,
  82                     ; 46 								DISABLE,
  82                     ; 47 								ADC1_ALIGN_LEFT,
  82                     ; 48 								ADC1_SCHMITTTRIG_ALL,
  82                     ; 49 								DISABLE);
  84  000a 4b00          	push	#0
  85  000c 4bff          	push	#255
  86  000e 4b00          	push	#0
  87  0010 4b00          	push	#0
  88  0012 4b00          	push	#0
  89  0014 4b00          	push	#0
  90  0016 7b07          	ld	a,(OFST+7,sp)
  91  0018 97            	ld	xl,a
  92  0019 4f            	clr	a
  93  001a 95            	ld	xh,a
  94  001b cd0000        	call	_ADC1_Init
  96  001e 5b06          	addw	sp,#6
  97                     ; 51 	ADC1_Cmd(ENABLE);
  99  0020 a601          	ld	a,#1
 100  0022 cd0000        	call	_ADC1_Cmd
 102                     ; 53 	return;
 105  0025 84            	pop	a
 106  0026 81            	ret	
 133                     ; 66 void csp_stm8s_adc1_deinit(void)
 133                     ; 67 {
 134                     .text:	section	.text,new
 135  0000               _csp_stm8s_adc1_deinit:
 139                     ; 68 	ADC1_Cmd(DISABLE);
 141  0000 4f            	clr	a
 142  0001 cd0000        	call	_ADC1_Cmd
 144                     ; 69 	ADC1_DeInit();												// DeInitialize ADC1
 146  0004 cd0000        	call	_ADC1_DeInit
 148                     ; 70 	CLK_PeripheralClockConfig(CLK_PERIPHERAL_ADC, DISABLE);		// Disable ADC1 clock
 150  0007 ae1300        	ldw	x,#4864
 152                     ; 71 	return;
 155  000a cc0000        	jp	_CLK_PeripheralClockConfig
 213                     ; 86 uint16_t csp_stm8s_adc1_rd12(uint8_t channel)
 213                     ; 87 {
 214                     .text:	section	.text,new
 215  0000               _csp_stm8s_adc1_rd12:
 217  0000 5203          	subw	sp,#3
 218       00000003      OFST:	set	3
 221                     ; 88 	uint8_t		regindex	= 0;
 223  0002 0f01          	clr	(OFST-2,sp)
 224                     ; 89 	uint16_t	result		= 0;
 226                     ; 91 	csp_stm8s_adc1_init(channel);
 228  0004 cd0000        	call	_csp_stm8s_adc1_init
 230                     ; 93 	ADC1_StartConversion();							// Start ADC1 Conversion using Software trigger
 232  0007 cd0000        	call	_ADC1_StartConversion
 235  000a               L76:
 236                     ; 95 	while(ADC1_GetFlagStatus(ADC1_FLAG_EOC) == 0);	// Wait until End-Of-Convertion	(1= end of conversion)
 238  000a a680          	ld	a,#128
 239  000c cd0000        	call	_ADC1_GetFlagStatus
 241  000f 4d            	tnz	a
 242  0010 27f8          	jreq	L76
 243                     ; 98 	result = ADC1_GetConversionValue();
 245  0012 cd0000        	call	_ADC1_GetConversionValue
 247  0015 1f02          	ldw	(OFST-1,sp),x
 248                     ; 100 	csp_stm8s_adc1_deinit();
 250  0017 cd0000        	call	_csp_stm8s_adc1_deinit
 252                     ; 102 	return(result);
 254  001a 1e02          	ldw	x,(OFST-1,sp)
 257  001c 5b03          	addw	sp,#3
 258  001e 81            	ret	
 271                     	xdef	_csp_stm8s_adc1_rd12
 272                     	xdef	_csp_stm8s_adc1_deinit
 273                     	xdef	_csp_stm8s_adc1_init
 274                     	xref	_CLK_PeripheralClockConfig
 275                     	xref	_ADC1_GetFlagStatus
 276                     	xref	_ADC1_GetConversionValue
 277                     	xref	_ADC1_StartConversion
 278                     	xref	_ADC1_Cmd
 279                     	xref	_ADC1_Init
 280                     	xref	_ADC1_DeInit
 299                     	end
