   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.10.2 - 02 Nov 2011
   3                     ; Generator (Limited) V4.3.7 - 29 Nov 2011
   4                     ; Optimizer V4.3.6 - 29 Nov 2011
  64                     ; 38 void csp_STM8S_spi1_config(uint8_t on_off)
  64                     ; 39 {
  66                     .text:	section	.text,new
  67  0000               _csp_STM8S_spi1_config:
  71                     ; 41 	if(on_off){
  73  0000 4d            	tnz	a
  74  0001 271d          	jreq	L72
  75                     ; 42 		CLK_PeripheralClockConfig(CLK_PERIPHERAL_SPI, ENABLE);
  77  0003 ae0101        	ldw	x,#257
  78  0006 cd0000        	call	_CLK_PeripheralClockConfig
  80                     ; 43 		SPI_Init(	SPI_FIRSTBIT_MSB,
  80                     ; 44 					SPI_BAUDRATEPRESCALER_2,
  80                     ; 45 					SPI_MODE_MASTER, 
  80                     ; 46 					SPI_CLOCKPOLARITY_LOW,
  80                     ; 47 					SPI_CLOCKPHASE_1EDGE,
  80                     ; 48 					SPI_DIRECTION_TX,
  80                     ; 49 					SPI_NSS_SOFT,
  80                     ; 50 					SPI_CRC_RX);
  82  0009 4b00          	push	#0
  83  000b 4b02          	push	#2
  84  000d 4b01          	push	#1
  85  000f 4b00          	push	#0
  86  0011 4b00          	push	#0
  87  0013 4b04          	push	#4
  88  0015 5f            	clrw	x
  89  0016 cd0000        	call	_SPI_Init
  91  0019 5b06          	addw	sp,#6
  92                     ; 51 		SPI_Cmd(ENABLE);
  94  001b a601          	ld	a,#1
  98  001d cc0000        	jp	_SPI_Cmd
  99  0020               L72:
 100                     ; 54 		SPI_Cmd(DISABLE);
 102  0020 cd0000        	call	_SPI_Cmd
 104                     ; 55 		SPI_DeInit();
 106  0023 cd0000        	call	_SPI_DeInit
 108                     ; 56 		CLK_PeripheralClockConfig(CLK_PERIPHERAL_SPI, DISABLE);	
 110  0026 ae0100        	ldw	x,#256
 112                     ; 58 	return;
 115  0029 cc0000        	jp	_CLK_PeripheralClockConfig
 152                     ; 71 uint8_t csp_STM8S_spi1_rdwr(uint8_t data)
 152                     ; 72 {
 153                     .text:	section	.text,new
 154  0000               _csp_STM8S_spi1_rdwr:
 156  0000 88            	push	a
 157       00000000      OFST:	set	0
 160  0001               L35:
 161                     ; 73 	while((SPI->SR & SPI_SR_TXE) == 0);		//bit hi when empty
 163  0001 72035203fb    	btjf	20995,#1,L35
 164                     ; 74 	SPI_SendData(data);
 166  0006 7b01          	ld	a,(OFST+1,sp)
 167  0008 cd0000        	call	_SPI_SendData
 170  000b               L16:
 171                     ; 75 	while((SPI->SR & SPI_SR_RXNE) == 0);	//bit low when rx buffer empty
 173  000b 72015203fb    	btjf	20995,#0,L16
 174                     ; 76 	return (SPI_ReceiveData());
 176  0010 cd0000        	call	_SPI_ReceiveData
 180  0013 5b01          	addw	sp,#1
 181  0015 81            	ret	
 194                     	xdef	_csp_STM8S_spi1_rdwr
 195                     	xdef	_csp_STM8S_spi1_config
 196                     	xref	_SPI_ReceiveData
 197                     	xref	_SPI_SendData
 198                     	xref	_SPI_Cmd
 199                     	xref	_SPI_Init
 200                     	xref	_SPI_DeInit
 201                     	xref	_CLK_PeripheralClockConfig
 220                     	end
