/*
*********************************************************************************************************
*                                           Marturion Ltd
*
*                                           Knockmore Hill Business Park
*                                           9 Ferguson Drive
*                                           Lisburn
*                                           Co. Antrim
*                                           Northern Ireland
*                                           BT28 2EX
*
*
*                   Copyright 2020, Marturion Electronics Ltd, Lisburn, Co. Antrim, Northern Ireland
*                                          All Rights Reserved
*
*                                          main.c
*
* Filename    :  main.c
* Programmer  :  Steven Surgenor
* Description :  Main Application file.
* Compiler    :  Cosmic Complier
* Target      :  STM8S003
* ST Library  :  Version 0.0.1
*********************************************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "stm8s_exti.h"
#include "stm8s_tim1.h"
#include "stm8s_beep.h"
#include "csp_STM8S_uart1.h"
#include "pcb_startup.h"
#include "pcb_pins.h"
#include "globals.h"

/* Private functions ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Global variables ----------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/
void stm8s_init(void);
void WriteFlash(u32 add, u8 *data_in);
void ReadFlash(u32 add, u8 *data_out);
void check_eeprom_init(void);
uint8_t Delay(uint16_t count);
void LED_strip(uint8_t enable);
/* Public variables ---------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
#define	nop_delay_05();		nop();		nop();		nop();		nop();		nop();
#define	nop_delay_10();		nop_delay_05();		nop_delay_05();
#define	nop_delay_20();		nop_delay_10();		nop_delay_10();
#define	nop_delay_50();		nop_delay_20();		nop_delay_20();		nop_delay_10();
#define	nop_delay_100();		nop_delay_50();		nop_delay_50();
#define	nop_delay_200();		nop_delay_100();		nop_delay_100();
#define	nop_delay_500();		nop_delay_200();		nop_delay_200();		nop_delay_100();

#define ASM _asm


	#define STARTUP_SWIM_DELAY_1S_1 \
 { \
 ASM(" PUSHW X \n" \
 " PUSH A \n" \
 " LDW X, #0xFFFF \n" \
 "loop1: LD A, #10 \n" \
 \
 "loop2: DEC A \n" \
 " JRNE loop2 \n" \
 \
 " DECW X \n" \
 " JRNE loop1 \n" \
 \
 " POP A \n" \
 " POPW X " ); \
 }
 
 	#define STARTUP_SWIM_DELAY_1S_2 \
 { \
 ASM(" PUSHW X \n" \
 " PUSH A \n" \
 " LDW X, #0xFFFF \n" \
 "loop3: LD A, #10 \n" \
 \
 "loop4: DEC A \n" \
 " JRNE loop4 \n" \
 \
 " DECW X \n" \
 " JRNE loop3 \n" \
 \
 " POP A \n" \
 " POPW X " ); \
 }


/*
*********************************************************************************************************
* Function Name : main
* Description   : This function is used for the main program.
* Arguments     : None
* Returns       : None
* Notes         : None
*
* Version		Date d/m/Y	    	Programmer          Reason for Change
* 1.0.0		 	31/01/2020 	    	Steven Surgenor	    Original Created
*********************************************************************************************************
*/
void main(void)
{
	// Local Variables
	uint16_t		i;
	uint32_t    count;
	uint32_t 		max_count = 2499;
	
	// delay to allow MCU programming
	for(i=0;i<2000;i++)
	{
		nop_delay_500();
	}
	pcb_startup();
	
	// turn on/off fet every second - 5000 clicks 
	for(count=0;count<=2499;count++)
		{
		fetWr(1); // turn fet on
		
		STARTUP_SWIM_DELAY_1S_1 // delay 1 sec
		
		fetWr(0); // turn fet off
		
		STARTUP_SWIM_DELAY_1S_2		// delay 1 sec

	// turn on LED when 5000 ticks have elapsed
	if (count >= max_count)
		{
		ledOn(1); // turn led on
			}
}

}
		
// end of main


/****************** (c) 2008  STMicroelectronics ******************************/

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval
  * None
  */
void assert_failed(u8* file, u32 line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif
