/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_clk.h
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#ifndef _CSP_STM8L_CLK_H
#define _CSP_STM8L_CLK_H

void csp_STM8S_clk_LSI(uint8_t state);

void csp_STM8S_clk_HSE(uint8_t state);
void csp_STM8S_clk_HSI(uint8_t state);


#endif
/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

