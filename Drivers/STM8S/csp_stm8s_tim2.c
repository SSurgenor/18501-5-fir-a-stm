/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_tim2.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#include "stm8s.h"

#include "csp_STM8S_tim2.h"
#include "stm8s_clk.h"
#include "stm8s_tim2.h"

/**********************************************************************************************************
 * Function Name : csp_stm8s_tim2_init
 * Description   : This function is used to initialise the ADC module
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0			11/03/14		William Paul		Original Created
 **********************************************************************************************************/
void csp_stm8s_tim2_init(void)
{
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER2, ENABLE);
	TIM2_DeInit();
	
	TIM2_TimeBaseInit(TIM2_PRESCALER_16, 1000);
						
	TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
	
	TIM2_Cmd(ENABLE);		
	return;
}


/*********************************************************************************************************
 *********************************************************************************************************/
//end of file
