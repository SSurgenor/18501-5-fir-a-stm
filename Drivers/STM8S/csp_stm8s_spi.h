/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_stm8s_spi.h
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#ifndef __CSP_STM8L_SPI_H
#define __CSP_STM8L_SPI_H


void		csp_STM8S_spi1_config(uint8_t on_off);
uint8_t	csp_STM8S_spi1_rdwr(uint8_t data);

#endif
/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

