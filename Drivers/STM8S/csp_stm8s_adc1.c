/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_adc1.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#include "stm8s.h"

#include "csp_STM8S_adc1.h"
#include "stm8s_adc1.h"
#include "stm8s_clk.h"


/**********************************************************************************************************
 * Function Name : csp_stm8s_adc1_init
 * Description   : This function is used to initialise the ADC module
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		13/11/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_stm8s_adc1_init(uint8_t channel)
{
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_ADC, ENABLE);
	ADC1_DeInit();
								
	ADC1_Init(					ADC1_CONVERSIONMODE_SINGLE,
								channel,
								ADC1_PRESSEL_FCPU_D2,
								ADC1_EXTTRIG_TIM,
								DISABLE,
								ADC1_ALIGN_LEFT,
								ADC1_SCHMITTTRIG_ALL,
								DISABLE);
	
	ADC1_Cmd(ENABLE);

	return;
}

/**********************************************************************************************************
 * Function Name : csp_stm8s_adc1_deinit
 * Description   : This function is used to shutdown the ADC module
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0			11/03/14	William Paul	Original Created
 **********************************************************************************************************/
void csp_stm8s_adc1_deinit(void)
{
	ADC1_Cmd(DISABLE);
	ADC1_DeInit();												// DeInitialize ADC1
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_ADC, DISABLE);		// Disable ADC1 clock
	return;
}


/**********************************************************************************************************
 * Function Name :	csp_stm8s_adc1_rd12
 * Description   : This function is used to read an DC channel
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y		Programmer		Reason for Change
 * 1.0.0		13/11/12		William Paul	Original Created
 * 2.0.0		08/04/15		Tim Barr		Modified for STM8S
 **********************************************************************************************************/
uint16_t csp_stm8s_adc1_rd12(uint8_t channel)
{
	uint8_t		regindex	= 0;
	uint16_t	result		= 0;
	
	csp_stm8s_adc1_init(channel);

	ADC1_StartConversion();							// Start ADC1 Conversion using Software trigger

	while(ADC1_GetFlagStatus(ADC1_FLAG_EOC) == 0);	// Wait until End-Of-Convertion	(1= end of conversion)

	// Return conversion value
	result = ADC1_GetConversionValue();
	
	csp_stm8s_adc1_deinit();

	return(result);
}


/*********************************************************************************************************
 *********************************************************************************************************/
//end of file
