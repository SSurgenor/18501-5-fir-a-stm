/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_tim2.h
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#ifndef __CSP_STM8L_TIM2_H
#define __CSP_STM8L_TIM2_H


void 	csp_stm8s_tim2_init(void);

#endif
/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

