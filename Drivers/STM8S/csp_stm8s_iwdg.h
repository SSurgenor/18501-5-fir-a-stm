/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_iwdg.h
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#ifndef __CSP_STM8L_IWDG_H
#define __CSP_STM8L_IWDG_H


void		csp_stm8s_iwdg_init(void);
void 		csp_stm8s_iwdg_ReloadCounter(void);



#define		IWDG_WRITE_ACCESS	0x55


#endif
/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

