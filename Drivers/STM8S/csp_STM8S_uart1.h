/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_uart1.h
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#ifndef _CSP_STM8S_UART1_H
#define _CSP_STM8S_UART1_H

#include "pcb_uart1.h"



#if(UART1_EN == 1)


#define BASE_10		10
#define BASE_16		16

/*********************************************************************************************************
 *		Typedefs
 *********************************************************************************************************/
typedef struct{
	uint8_t				wr_index_u8;
	uint8_t				rd_index_u8;
	uint8_t				cnt_u8;

}t_serial_st;

/*********************************************************************************************************
 *		Global Vars
 *********************************************************************************************************/
extern t_serial_st		st_uart1_rx_glb;
extern uint8_t			uart1_rx_buffer_a_u8_glb[UART1_RX_BUFFER_SIZE];

/*********************************************************************************************************
 *		Function prototypes
 *********************************************************************************************************/
void		csp_STM8S_uart1_config(void);

void		csp_STM8S_uart_rx_interrupt_handler(void);

uint8_t		csp_STM8S_uart1_getchar(uint8_t *rec_status_u8);
void		csp_STM8S_uart1_putchar(uint8_t out_char);

void 		csp_STM8S_uart1_putstr(char *str);
void 		csp_STM8S_uart1_putnum(uint32_t num,uint8_t base,uint8_t len);
uint32_t	csp_STM8S_uart1_getnum(uint8_t base);

#endif	//if(UART1_EN == 1)

#endif
/*********************************************************************************************************
 *********************************************************************************************************/
//end of file
