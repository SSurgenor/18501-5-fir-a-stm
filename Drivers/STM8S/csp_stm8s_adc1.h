/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_adc1.h
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#ifndef __CSP_STM8L_ADC1_H
#define __CSP_STM8L_ADC1_H


void			csp_stm8s_adc1_init(uint8_t channel);
void			csp_stm8s_adc1_deinit(void);

uint16_t		csp_stm8s_adc1_rd12(uint8_t channel);


#endif
/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

