/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_uart1.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#include "stm8s.h"
#include "stm8s_clk.h"

#include "csp_STM8S_uart1.h"

#include "pcb_uart1.h"

#include "stm8s_uart1.h"


#if(UART1_EN == 1)

t_serial_st		st_uart1_rx_glb;
uint8_t			uart1_rx_buffer_a_u8_glb[UART1_RX_BUFFER_SIZE];

/**********************************************************************************************************
 * Function Name : csp_STM8S_uart1_config
 * Description   : This function is used to
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		13/11/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_uart1_config(void)
{
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_UART1, ENABLE);
	
	UART1_Init(	BAUD_RATE,
				UART1_WORDLENGTH_8D,
				UART1_STOPBITS_1,
				UART1_PARITY_NO,
				UART1_SYNCMODE_CLOCK_DISABLE,
				UART1_MODE_TXRX_ENABLE);

	st_uart1_rx_glb.rd_index_u8		=  0;
	st_uart1_rx_glb.wr_index_u8		=  0;
	st_uart1_rx_glb.cnt_u8			=  0;

	// Enable the USART Receive interrupt
  	UART1_ITConfig(UART1_IT_RXNE, ENABLE);			// this interrupt is generated when the USART receive data register is not empty
	// Enable the USART Transmit complete interrupt
//	UART_ITConfig(UART1, UART1_IT_TC, ENABLE);			// this interrupt is generated when the USART transmit Shift Register is empty

	// Enable USART
	UART1_Cmd(ENABLE);

	enableInterrupts();										// Enable general interrupts
	return;
}



/**********************************************************************************************************
 * Function Name : csp_STM8S_uart_rx_interrupt_handler
 * Description   : This function is used to
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		13/11/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_uart_rx_interrupt_handler(void)
{
	uart1_rx_buffer_a_u8_glb[st_uart1_rx_glb.wr_index_u8]	=  UART1->DR;
	if(++st_uart1_rx_glb.wr_index_u8 >= UART1_RX_BUFFER_SIZE){
		st_uart1_rx_glb.wr_index_u8	=  0;
	}
	disableInterrupts();
	st_uart1_rx_glb.cnt_u8++;
	enableInterrupts();

}



/**********************************************************************************************************
 * Function Name : csp_STM8S_uart1_getchar
 * Description   : This function is used to get a character from the receive buffer on UART1
 * Arguments     :	uint8_t		*rec_status_u8
 * Returns       :	uint8_t	 	data_byte_u8
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		13/11/12	William Paul	Original Created
 **********************************************************************************************************/
uint8_t csp_STM8S_uart1_getchar(uint8_t *rec_status_u8)
{
	uint8_t 		data_byte_u8 	= 0;

	*rec_status_u8	= 0;

	if(st_uart1_rx_glb.cnt_u8 != 0){
		data_byte_u8		= uart1_rx_buffer_a_u8_glb[st_uart1_rx_glb.rd_index_u8];
		*rec_status_u8		= 1;

		if (++st_uart1_rx_glb.rd_index_u8 >= UART1_RX_BUFFER_SIZE){
			st_uart1_rx_glb.rd_index_u8	=  0;
		}
		disableInterrupts();
		st_uart1_rx_glb.cnt_u8--;
		enableInterrupts();

	}

   	return( (char)data_byte_u8 );
}



/**********************************************************************************************************
 * Function Name : csp_STM8S_uart1_putchar
 * Description   : This function is used to output a character on UART1
 * Arguments     : char		out_char
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		13/11/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_uart1_putchar(uint8_t out_char)
{
	uint8_t		i;
	volatile uint8_t	status;
#if(UART1_IRDA_EN == 1)
	UART1->CR2 &= ~UART1_CR2_REN;	//0 disabled/1 enabled transmitter
	UART1->CR2 |=  UART1_CR2_TEN;	//0 disabled/1 enabled transmitter
//	for(i=0;i<50;i++){nop();}
#endif
	
	UART1_SendData8(out_char);
//	while(UART1->SR & UART_FLAG_TC == 0x00);
	do{
		status	=  UART1->SR;
		status	&= UART1_FLAG_TC;
	}while(status == 0);

#if(UART1_IRDA_EN == 1)
//	for(i=0;i<50;i++){nop();}
	UART1->CR2 &= ~UART1_CR2_TEN;	//0 disabled/1 enabled transmitter
	UART1->CR2 |=  UART1_CR2_REN;	//0 disabled/1 enabled transmitter
#endif
   	return;
}


/**********************************************************************************************************
 * Function Name : csp_STM8S_uart1_putstr
 * Description   : This function is used to output a string on UART1
 * Arguments     : char		out_char
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		13/11/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_uart1_putstr(char *str)
{
	do{
		csp_STM8S_uart1_putchar(*str);
		str++;
	}while(*str != 0x00);

   	return;
}

/**********************************************************************************************************
 * Function Name : csp_STM8S_uart1_putnum
 * Description   : This function is used to output a number on UART1
 * Arguments     :	uint32_t	num
 *					uint8_t		base
 * 					uint8_t		len
 * Returns       : void
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		16/04/13	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_uart1_putnum(uint32_t num,uint8_t base,uint8_t len)
{
	char		str[9];
	uint32_t	temp;
	uint8_t		i;
	
	for(i=len;i>0;i--){
		temp	=  num%base;
		num		/= base;
		str[i]	=  (char)temp;
	}
	for(i=1;i<=len;i++){
		if(str[i] <= 9){	csp_STM8S_uart1_putchar(str[i]+'0');	}
		else{				csp_STM8S_uart1_putchar(str[i]+'A'-10);	}
	}
   	return;
}

/**********************************************************************************************************
 * Function Name : csp_STM8S_uart1_getnum
 * Description   : This function is used to input an ascii num
 * Arguments     : void
 * Returns       : uint32_t	num
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		13/11/12	William Paul	Original Created
 **********************************************************************************************************/
uint32_t csp_STM8S_uart1_getnum(uint8_t base)
{
	uint32_t	num			=  0;
	uint8_t		rx_byte;
	uint8_t		rx_status;
	uint8_t		non_dig		=  0;

	do{
		rx_byte		=  csp_STM8S_uart1_getchar(&rx_status);
		if(rx_status){
			if((rx_byte >= '0')&&(rx_byte <= '9')){
				num		*= base;
				num		+= rx_byte;
				num		-= '0';
			}
			else if(base == 16){
				if((rx_byte >= 'A')&&(rx_byte <= 'F')){
					num		*= base;
					num		+= rx_byte;
					num		-= 'A';
					num		+= 10;
				}
				else if((rx_byte >= 'a')&&(rx_byte <= 'f')){
					num		*= base;
					num		+= rx_byte;
					num		-= 'a';
					num		+= 10;
				}
				else{
					non_dig	=  1;
				}
			}
			else{
				non_dig	=  1;
			}
			csp_STM8S_uart1_putchar(rx_byte);
		}
	}while(non_dig == 0);

   	return(num);
}



#endif	//#if(UART1_EN == 1)

/*********************************************************************************************************
 *********************************************************************************************************/
//end of file
