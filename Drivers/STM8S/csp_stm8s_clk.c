/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  STM8L_clk.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#include "stm8s.h"
#include "stm8s_clk.h"

#include "csp_STM8S_clk.h"


/**********************************************************************************************************
 * Function Name : csp_STM8S_clk_LSI
 * Description   : This function is used to enable the LSI	(external 38kHz clock)
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		29/10/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_clk_LSI(uint8_t state)
{
	if(state){
		CLK_LSICmd(ENABLE);										// Enable LSI
		while (CLK_GetFlagStatus(CLK_FLAG_LSIRDY) == RESET);	// Wait for LSI clock to be ready
	}
	else{
		CLK_LSICmd(DISABLE);									// Disable LSI
	}
	//may want to wait for clock to stabilise here

	return;
}

/**********************************************************************************************************
 * Function Name : csp_STM8S_clk_HSE
 * Description   : This function is used to enable the HSE	(external fast Hz clock)
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		29/10/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_clk_HSE(uint8_t state)
{
	if(state){
		CLK_HSECmd(ENABLE);										// Enable HSE
		while (CLK_GetFlagStatus(CLK_FLAG_HSERDY) == RESET);	// Wait for HSE clock to be ready
	}
	else{
		CLK_HSECmd(DISABLE);									// Disable HSE
	}
	//may want to wait for clock to stabilise here

	return;
}

/**********************************************************************************************************
 * Function Name : csp_STM8S_clk_HSI
 * Description   : This function is used to enable the HSI	(internal 16MHz clock)
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		29/10/12	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_clk_HSI(uint8_t state)
{
	if(state){
		CLK_HSICmd(ENABLE);										// Enable HSI
		while (CLK_GetFlagStatus(CLK_FLAG_HSIRDY) == RESET);	// Wait for HSI clock to be ready
	}
	else{
		CLK_HSICmd(DISABLE);									// Disable HSI
	}
	//may want to wait for clock to stabilise here

	return;
}

/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

