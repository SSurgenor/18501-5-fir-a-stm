/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_STM8S_iwdg.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#include "stm8s.h"
#include "stm8s_iwdg.h"

#include "csp_STM8S_iwdg.h"
#include "csp_STM8S_clk.h"



/**********************************************************************************************************
 * Function Name : csp_stm8s_iwdg_init
 * Description   : This function is used to initialise the watchdog module
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		08/01/13	William Paul	Original Created
 **********************************************************************************************************/
void csp_stm8s_iwdg_init(void)
{
	//Code not tested
	
	csp_STM8S_clk_LSI(1);							//internal 128kHz clk enable
	
	IWDG_WriteAccessCmd(IWDG_WRITE_ACCESS);			//write access command

	IWDG_SetPrescaler(IWDG_Prescaler_128);			//set prescalar   
		//LSI clk    		=  38000Hz
		//128000Hz	/ 128	=  1000Hz
		//1000Hz 	/ 100	=  10Hz 				watchdog reset period
	IWDG_SetReload(100);							//set reload
	
	csp_stm8s_iwdg_ReloadCounter();					//comand to reload the counddown timer
	IWDG_Enable();
	
	return;
}


/**********************************************************************************************************
 * Function Name : csp_stm8s_iwdg_ReloadCounter
 * Description   : This function is used to initialise the watchdog module
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		08/01/13	William Paul	Original Created
 **********************************************************************************************************/
void csp_stm8s_iwdg_ReloadCounter(void)
{
	//CoDE NOT TESTED
	
  IWDG->KR = IWDG_KEY_REFRESH;
}



/*********************************************************************************************************
 *********************************************************************************************************/
//end of file
