/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  csp_stm8s_spi.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#include "stm8s.h"
#include "stm8s_spi.h"
#include "stm8s_clk.h"
#include "stm8s_spi.h"

#include "csp_stm8s_spi.h"


/**********************************************************************************************************
 * Function Name : csp_STM8S_spi1_config
 * Description   : This function is used to configure the SPI port
 * Arguments     : uint8_t on_off
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		26/02/13	William Paul	Original Created
 **********************************************************************************************************/
void csp_STM8S_spi1_config(uint8_t on_off)
{

	if(on_off){
		CLK_PeripheralClockConfig(CLK_PERIPHERAL_SPI, ENABLE);
		SPI_Init(	SPI_FIRSTBIT_MSB,
					SPI_BAUDRATEPRESCALER_2,
					SPI_MODE_MASTER, 
					SPI_CLOCKPOLARITY_LOW,
					SPI_CLOCKPHASE_1EDGE,
					SPI_DIRECTION_TX,
					SPI_NSS_SOFT,
					SPI_CRC_RX);
		SPI_Cmd(ENABLE);
	}
	else{
		SPI_Cmd(DISABLE);
		SPI_DeInit();
		CLK_PeripheralClockConfig(CLK_PERIPHERAL_SPI, DISABLE);	
	}
	return;
}

/**********************************************************************************************************
 * Function Name : csp_STM8S_spi1_rdwr
 * Description   : This function is used to configure the SPI port
 * Arguments     : uint8_t data
 * Returns       : uint8_t data
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		26/02/13	William Paul	Original Created
 **********************************************************************************************************/
uint8_t csp_STM8S_spi1_rdwr(uint8_t data)
{
	while((SPI->SR & SPI_SR_TXE) == 0);		//bit hi when empty
	SPI_SendData(data);
	while((SPI->SR & SPI_SR_RXNE) == 0);	//bit low when rx buffer empty
	return (SPI_ReceiveData());
}


/*********************************************************************************************************
 *********************************************************************************************************/
//end of file
