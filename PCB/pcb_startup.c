/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  pcb_startup.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/
#include <stdio.h>

#include "stm8s.h"
#include "stm8s_awu.h"
#include "stm8s_clk.h"
#include "stm8s_spi.h"
#include "stm8s_uart1.h"

//#include "csp_STM8S_rtc.h"
#include "csp_STM8S_clk.h"
#include "csp_STM8S_spi.h"
#include "csp_STM8S_tim2.h"
#include "csp_STM8S_uart1.h"

#include "pcb_pins.h"
#include "pcb_startup.h"

#include "globals.h"



/*********************************************************************************************************
 *		Include Files
 *********************************************************************************************************/


/*********************************************************************************************************
 *		Defines
 *********************************************************************************************************/


/**********************************************************************************************************
 * Function Name : pcb_startup
 * Description   : This function is used to
 * Arguments     : None
 * Returns       : None
 * Notes         : None
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		29/10/12	William Paul	Original Created
 **********************************************************************************************************/
void pcb_startup(void)
{
	// Local Variables

	// Code
	pcb_pins(1);
	//csp_STM8S_uart1_config();

	return;
}


/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

