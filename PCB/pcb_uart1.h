/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  pcb_uart1.h
 * Programmer  :  William Paul
 * Description :
 *
 **********************************************************************************************************/
#ifndef _PCB_UART1_H
#define _PCB_UART1_H


#define UART1_EN					1


#define UART1_IRDA_EN				1

#define	BAUD_RATE					9600

#define	UART1_RX_BUFFER_SIZE		10		// max 255

// UART2 init defines
#define WORDLENGTH_8D 			((u8)0x00)
#define STOPBITS_1 				((u8)0x00)
#define UART2_PARITY_NO 		((u8)0x00)
#define MODE_TXRX_ENABLE 		((u8)0x0C)
#define HSEfreq 				((u32)0xF42400) 	// fHSE = 16 MHz


#endif
/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

