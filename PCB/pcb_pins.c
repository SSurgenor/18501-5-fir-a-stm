/**********************************************************************************************************
 *  Marturion Ltd
 *
 *	Knockmore Hill Business Park
 *	9 Ferguson Drive
 *  Lisburn
 *  Co. Antrim
 *  Northern Ireland
 *  BT28 2EX
 *
 *  Copyright 2010, Marturion Ltd
 *  All Rights Reserved
 *
 *
 * Filename    :  pcb_pins.c
 * Programmer  :  William Paul
 * Description :  This module is used for all the uart functions.
 *
 **********************************************************************************************************/


/*********************************************************************************************************
 *		Include Files
 *********************************************************************************************************/
#include <stdio.h>
#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_exti.h"
#include "pcb_pins.h"

/*********************************************************************************************************
 *		Defines
 *********************************************************************************************************/



/**********************************************************************************************************
 * Function Name : pcb_pins
 * Description   : This function is used to
 * Arguments     : None
 * Returns       : None
 * Notes         :
 *					GPIO_PIN_0
 *					GPIO_PIN_1
 *					GPIO_PIN_2
 *					GPIO_PIN_3
 *					GPIO_PIN_4
 *					GPIO_PIN_5
 *					GPIO_PIN_6
 *					GPIO_PIN_7
 *					GPIO_PIN_LNIB
 *					GPIO_PIN_HNIB
 *					GPIO_PIN_All
 *
 *					GPIO_MODE_IN_FL_NO_IT      = (uint8_t)0x00,   Input floating, no external interrupt
 *					GPIO_MODE_IN_PU_NO_IT      = (uint8_t)0x40,   Input pull-up, no external interrupt
 *					GPIO_MODE_IN_FL_IT         = (uint8_t)0x20,   Input floating, external interrupt
 *					GPIO_MODE_IN_PU_IT         = (uint8_t)0x60,   Input pull-up, external interrupt
 *					GPIO_MODE_OUT_OD_LOW_FAST  = (uint8_t)0xA0,   Output open-drain, low level, 10MHz
 *					GPIO_MODE_OUT_PP_LOW_FAST  = (uint8_t)0xE0,   Output push-pull, low level, 10MHz
 *					GPIO_MODE_OUT_OD_LOW_SLOW  = (uint8_t)0x80,   Output open-drain, low level, 2MHz
 *					GPIO_MODE_OUT_PP_LOW_SLOW  = (uint8_t)0xC0,   Output push-pull, low level, 2MHz
 *					GPIO_MODE_OUT_OD_HIZ_FAST  = (uint8_t)0xB0,   Output open-drain, high-impedance level, 10MHz
 *					GPIO_MODE_OUT_PP_HIGH_FAST = (uint8_t)0xF0,   Output push-pull, high level, 10MHz
 *					GPIO_MODE_OUT_OD_HIZ_SLOW  = (uint8_t)0x90,   Output open-drain, high-impedance level, 2MHz
 *					GPIO_MODE_OUT_PP_HIGH_SLOW = (uint8_t)0xD0    Output push-pull, high level, 2MHz
 *
 *
 * Version		Date d/m/y	Programmer		Reason for Change
 * 1.0.0		29/10/12	William Paul	Original Created
 **********************************************************************************************************/
void pcb_pins(uint8_t enable)
{
	//GPIO_Init(GPIOA, 	GPIO_PIN_ALL, 	GPIO_MODE_IN_FL_NO_IT);			// Port A
	//GPIO_Init(GPIOB, 	GPIO_PIN_ALL, 	GPIO_MODE_IN_FL_NO_IT);			// Port B
	//GPIO_Init(GPIOC, 	GPIO_PIN_ALL, 	GPIO_MODE_IN_FL_NO_IT);			  // Port C
	//GPIO_Init(GPIOD, 	GPIO_PIN_ALL, 	GPIO_MODE_IN_FL_NO_IT);			// Port D
	//GPIO_Init(GPIOE, 	GPIO_PIN_ALL, 	GPIO_MODE_IN_FL_NO_IT);			// Port E
	//GPIO_Init(GPIOF, 	GPIO_PIN_ALL, 	GPIO_MODE_IN_FL_NO_IT);			// Port F

	if(enable){
// PORT A
		GPIO_Init(GPIOA,	GPIO_PIN_3,		GPIO_MODE_OUT_PP_LOW_FAST);	// TP

// PORT B
		// GPIO_Init(GPIOB,	GPIO_PIN_0,		GPIO_MODE_IN_PU_NO_IT);		//
		// GPIO_Init(GPIOB,	GPIO_PIN_1,		GPIO_MODE_IN_PU_NO_IT);		//
		// GPIO_Init(GPIOB,	GPIO_PIN_2,		GPIO_MODE_OUT_OD_LOW_FAST);	//
		// GPIO_Init(GPIOB,	GPIO_PIN_3,		GPIO_MODE_OUT_OD_LOW_FAST);	//
		 // GPIO_Init(GPIOB,	GPIO_PIN_4,		GPIO_MODE_OUT_OD_LOW_FAST);	// led
		// GPIO_Init(GPIOB,	GPIO_PIN_5,		GPIO_MODE_OUT_OD_LOW_FAST);	//
		// GPIO_Init(GPIOB,	GPIO_PIN_6,		GPIO_MODE_IN_PU_NO_IT);		//
//		GPIO_Init(GPIOB,	GPIO_PIN_7,		GPIO_MODE_OUT_PP_LOW_FAST);	//

// PORT C
		// GPIO_Init(GPIOC,	GPIO_PIN_1,		GPIO_MODE_OUT_PP_LOW_FAST);	//
		// GPIO_Init(GPIOC,	GPIO_PIN_2,		GPIO_MODE_OUT_PP_LOW_FAST);	//
		// GPIO_Init(GPIOC,	GPIO_PIN_3,		GPIO_MODE_OUT_PP_LOW_FAST);	//
		// GPIO_Init(GPIOC,	GPIO_PIN_4,		GPIO_MODE_OUT_PP_LOW_FAST);	//
			GPIO_Init(GPIOC,	GPIO_PIN_5,		GPIO_MODE_OUT_PP_LOW_FAST);	// FET_EN
		// GPIO_Init(GPIOC,	GPIO_PIN_6,		GPIO_MODE_OUT_PP_LOW_FAST);	//
		// GPIO_Init(GPIOC,	GPIO_PIN_7,		GPIO_MODE_OUT_PP_LOW_FAST);	//

// PORT D
		// GPIO_Init(GPIOD,	GPIO_PIN_0,		GPIO_MODE_OUT_PP_LOW_FAST);	//
//		GPIO_Init(GPIOD,	GPIO_PIN_1,		GPIO_MODE_OUT_PP_LOW_FAST);	//
		// GPIO_Init(GPIOD,	GPIO_PIN_2,		GPIO_MODE_OUT_PP_LOW_FAST);	//
		// GPIO_Init(GPIOD,	GPIO_PIN_3,		GPIO_MODE_OUT_PP_LOW_FAST);	//
		// GPIO_Init(GPIOD,	GPIO_PIN_4,		GPIO_MODE_OUT_PP_LOW_FAST);	//
//		GPIO_Init(GPIOD,	GPIO_PIN_5,		GPIO_MODE_OUT_PP_LOW_FAST);	//
//		GPIO_Init(GPIOD,	GPIO_PIN_6,		GPIO_MODE_OUT_PP_LOW_FAST);	//
//		GPIO_Init(GPIOD,	GPIO_PIN_7,		GPIO_MODE_OUT_PP_LOW_FAST);	//

// PORT E
		// GPIO_Init(GPIOE,	GPIO_PIN_5,		GPIO_MODE_OUT_PP_LOW_FAST);	//

// PORT F
//		GPIO_Init(GPIOF,	GPIO_PIN_4,		GPIO_MODE_OUT_PP_LOW_FAST);	//
	}

	return;
}


/*********************************************************************************************************
 *********************************************************************************************************/
//end of file

